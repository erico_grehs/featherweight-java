// Define a grammar called Fji que é o Featherweight com interfaces
grammar Fji;


// Featherweight Java com Interface Sintaxe:
//
// P 	::= INT' CL' e
// INT 	::= interface I extends I' {MS'}
// CL 	::= class C extends C [implements I'] {C' f'; K M'} 
// K 	::= C(C' f') {super(f'); this.f' = f'; }
// M 	::= C m(C' x'){return e}
// MS 	::= C m (C' x');
// e 	::= x 
//		| 	e.f 				
//		| 	e.m(e') 			
//		|	new C(e')   		
//		| 	(C) e				
//		|	this
//
// Onde a notação ' significa sequencia, podendo ter 0 ou n elementos. Como exemplo C'f' para C1f1, C2f2,...,Ck,fk).

// -----------------------------------------------------------------------------------------------------
// ------------------------------- Imports and options 	------------------------------------------------
// -----------------------------------------------------------------------------------------------------
@header {
	import java.util.*	;
	import java.io.*	;
}
@members {
	// Definicoes de escopo global //
	// CT(C) que é um mapeamento de um nome de classe C para uma declaracao onde classe CT [Capitulo 19].
	HashMap<String,String> ct = new HashMap<String,String>(); // Gerencia classe e superclasse
	HashMap<String,String> signature = new HashMap<String,String>(); 
	HashMap<String,String> descriptor = new HashMap<String,String>(); 
	HashMap<String,String> gb_construtores = new HashMap<String,String>(); 
	HashMap<String,String> gb_metodos = new HashMap<String,String>(); 
	HashMap<String,ArrayList<String>> it = new HashMap<String,ArrayList<String>>(); // interfaces
	ArrayList<String> m_args = new ArrayList<String>(); // Adicionado esta estrutura para resolver e ::= x (váriaveis locais)
}
	
// -----------------------------------------------------------------------------------------------------
// ----------------------------------- grammar parser --------------------------------------------------
// -----------------------------------------------------------------------------------------------------
prog	
@init {
// No começo são inicializadas 3 Hashmaps com a classe Object:
//	SIGNATURE - que guarda as assinaturas de cada elemento(classe, atributo e metodo) declarado
//	DESCRIPTOR - que guarda o descritor de cada elemento declarado
//	GB_CONSTRUTORES - que guarda a chamada do contrutor da classe para ser usada em super()
	signature.put("Object", "java/lang/Object");
	descriptor.put("Object", "Ljava/lang/Object;");
	gb_construtores.put("Object","java/lang/Object/<init>()V");

	PrintStream ps_console = System.out;

	File start = new File("Start.j");
}
:
	// -----------------------------------------------------------------------------------------------------
	inter*
	// -----------------------------------------------------------------------------------------------------
	cl+ 
	{

		// Gerar arquivo temporario onde será escrito o código.
		// Direcionar saida do println para este arquivo.

		// Store console print stream.
		
		try{
			FileOutputStream fos = new FileOutputStream(start);
			PrintStream ps = new PrintStream(fos);

			// Set file print stream.
			System.setOut(ps);
		}
		catch (IOException e) {


		}
		String interfacename = "";
	


		// Class Start é criada para a execução de t
		System.out.print(".class public Start\n");
		System.out.print(".super java/lang/Object\n");
		
		// PADRAO Jasmin
		System.out.print(".method public <init>()V\n");
		System.out.print("\taload_0\n");
		System.out.print("\tinvokenonvirtual java/lang/Object/<init>()V\n");
		System.out.print("\treturn\n");
		System.out.print(".end method\n\n");
		System.out.print(".method public static main([Ljava/lang/String;)V\n");
		System.out.print("\t.limit stack 10 \n");
		System.out.print("\t.limit locals 3 \n");
		System.out.print("\tgetstatic java/lang/System/out Ljava/io/PrintStream;\n");
	}
	// -----------------------------------------------------------------------------------------------------
	t
	{
		System.out.print("\tinvokevirtual java/io/PrintStream/println(Ljava/lang/Object;)V\n");
		System.out.print("\treturn\n");
		System.out.print(".end method\n\n");


		System.setOut(ps_console);


	}
	// -----------------------------------------------------------------------------------------------------
;

// -----------------------------------------------------------------------------------------------------
// ----------------------------------- regra interface(inter) ------------------------------------------
// -----------------------------------------------------------------------------------------------------
inter 
//
// interface I extends I' {MS}
//
// 'interface' intname=I ('extends' extintname=I (',' extintname=I)*)? '{' MS* '}' 
//
	// -----------------------------------------------------------------------------------------------------
	@init
	{
		// Gerar arquivo temporario onde será escrito o código.
		// Direcionar saida do println para este arquivo.

		// Store console print stream.
		PrintStream ps_console = System.out;

		File file = new File("temp.Fji");
		try{
			FileOutputStream fos = new FileOutputStream(file);
			PrintStream ps = new PrintStream(fos);

			// Set file print stream.
			System.setOut(ps);
		}
		catch (IOException e) {


		}
		String interfacename = "";

	}
	// -----------------------------------------------------------------------------------------------------
	@after
	{
	       // Set console print stream.
	       File newFile = new File(interfacename + ".j");
	       file.renameTo(newFile);
           System.setOut(ps_console);
	}
	// -----------------------------------------------------------------------------------------------------
	:
	// -----------------------------------------------------------------------------------------------------
	'interface' intname=C 
	{
		System.out.print(".interface public " + $intname.text + "\n");
		System.out.print(".super java/lang/Object\n"); // Por mais que nao faça sentido, nos testes se mostrou OBRIGATORIO ter um .super em interfaces.
		
		ArrayList<String> extendints = new ArrayList<String>();
		extendints.clear();
    	it.put($intname.text, extendints);

		signature.put($intname.text, $intname.text);
		descriptor.put($intname.text, "L"+$intname.text+";");

		interfacename = $intname.text;
	}

	// -----------------------------------------------------------------------------------------------------
	
	('extends' extintname=C 
	{	
		if (it.get($extintname.text) != null){
			// Interface já foi declarada 
			System.out.print(".implements " + $extintname.text + "\n");
			extendints = it.get($intname.text);
			extendints.add($extintname.text);

		}
		else {
			// Se a interface nao foi declarada ainda, considera que é Object
			System.out.print(".implements java/lang/Object\n");		
			extendints = it.get($intname.text);
			extendints.add("java/lang/Object");
		}
	}

	// -----------------------------------------------------------------------------------------------------
	
	(',' extintname=C
	{
		if (it.get($extintname.text) != null){
			// Interface já foi declarada 
			System.out.print(".implements " + $extintname.text + "\n");
			extendints = it.get($intname.text);
			extendints.add($extintname.text);

		}
		else {
			// Se a interface nao foi declarada ainda, considera que é Object
			System.out.print(".implements java/lang/Object\n");		
			extendints = it.get($intname.text);
			extendints.add("java/lang/Object");
		}

	}

	)*
	)? 
	// -----------------------------------------------------------------------------------------------------
	'{'
	// -----------------------------------------------------------------------------------------------------
	ms*
	// -----------------------------------------------------------------------------------------------------
	'}'
	// -----------------------------------------------------------------------------------------------------
;

// -----------------------------------------------------------------------------------------------------
// ----------------------------------- regra classe(cl) ------------------------------------------------
// -----------------------------------------------------------------------------------------------------
cl	
	// -----------------------------------------------------------------------------------------------------
	@init {
		HashMap<String,String> fields = new HashMap<String,String>();
	
		// Gerar arquivo temporario onde será escrito o código.
		// Direcionar saida do println para este arquivo.
		// Store console print stream.
		PrintStream ps_console = System.out;

		File file = new File("temp.Fji");
		try{
			FileOutputStream fos = new FileOutputStream(file);
			PrintStream ps = new PrintStream(fos);

			// Set file print stream.
			System.setOut(ps);
		}
		catch (IOException e) {

		}
		String classname = "";

	}
	// -----------------------------------------------------------------------------------------------------
	@after
	{
	       // Set console print stream.
	       File newFile = new File(classname + ".j");
	       file.renameTo(newFile);
           System.setOut(ps_console);
	}
:	
	//---------------------------------------------------------------------------------------------------
	'class' cname=C 
	{
		System.out.print(".class public "+$cname.text + "\n");
		classname = $cname.text;
	}
	//---------------------------------------------------------------------------------------------------
	'extends' ext=C 
	{
		if ($ext.text.equals("Object"))
		{
			System.out.print(".super java/lang/Object\n");
		}
		else 
		{
			if (ct.get($ext.text) != null)
			{
				// Classe existe
				System.out.print(".super" + $ext.text+ "\n");
			}
			else 
			{
				// Se a classe nao foi declarada ainda considera Object
				System.out.print(".super java/lang/Object\n");		
			}
		}
		// adiciona classe e sua respectiva superclasse
		ct.put($cname.text, $ext.text); 
		
		signature.put($cname.text, $cname.text);
		descriptor.put($cname.text, "L"+$cname.text+";");

	}
	
	//---------------------------------------------------------------------------------------------------
	// se a classe implementa interfaces [uma ou nenhuma]
	( 'implements' extintname=C 
	{

		if (it.get($extintname.text) != null){
			// Interface já foi declarada 
			System.out.print(".implements " + $extintname.text + "\n");
		}
		else {
			// Se a interface nao foi declarada ainda considera Object
			System.out.print(".implements java/lang/Object\n");		
			// /*DEBUG*/System.out.println("extendints: " + extendints);
		}
	}
	//---------------------------------------------------------------------------------------------------
	// Se a classe implementa mais de uma interface
	(',' extintname=C
	{
		if (it.get($extintname.text) != null){
			// Interface já foi declarada 
			System.out.print(".implements " + $extintname.text + "\n");
		}
		else {
			// Se a interface nao foi declarada ainda considera Object
			System.out.print(".implements java/lang/Object\n");		
		}

	}

	)*
	//---------------------------------------------------------------------------------------------------
	)?	
	//---------------------------------------------------------------------------------------------------
	'{' 
	//---------------------------------------------------------------------------------------------------
	// Definicao dos atributos da classe [nenhum ou n]
	(
	ftype=C fname=ID ';' 
	{
		fields.put($fname.text , $ftype.text); 
		
		System.out.print(".field public "+ $fname.text + " "+ descriptor.get($ftype.text) +"\n");
		// Estrutura auxiliar
		// Insere atributos 
		signature.put($fname.text, $cname.text + "/" + $fname.text);
		descriptor.put($fname.text, descriptor.get($ftype.text));

	}
	)* 
	//---------------------------------------------------------------------------------------------------
	// Para a definicao do construtor chama k
	k
	//---------------------------------------------------------------------------------------------------
	// Definicao dos metodos da classe, alem do construtor, passando como atributo o nome da classe [nenhum ou n]
	(
	m[$cname.text]
	)* 
	//---------------------------------------------------------------------------------------------------
	'}'	
;
// -----------------------------------------------------------------------------------------------------
// ----------------------------------- regra contrutor(k) ----------------------------------------------
// -----------------------------------------------------------------------------------------------------
k
@init {
	ArrayList<String> k_args = new ArrayList<String>();
	int numsuperargs = 0;
	int numargs = 0;
	k_args.add("this");
	String sig_construct = "";
}		
: 
	// -----------------------------------------------------------------------------------------------------
	// C
	//
	clname=C 
	{
		sig_construct= ""; 
		sig_construct+= signature.get($clname.text) + "/<init>";
		System.out.print(".method public <init>");
	}
	// -----------------------------------------------------------------------------------------------------
	// (
	//
		'('
		{
			sig_construct+= "(";
			System.out.print("(");
		}
	// -----------------------------------------------------------------------------------------------------
	// C' f' 
			(
				argtype=C arg=ID 
				{
					k_args.add($arg.text); 
					numargs++;
					sig_construct+= descriptor.get($argtype.text);
					System.out.print(descriptor.get($argtype.text));
				}
				(
	// -----------------------------------------------------------------------------------------------------
	// , C' f'
					',' argtype=C arg=ID 
					{
						k_args.add($arg.text); 
						numargs++;
						sig_construct+= descriptor.get($argtype.text);
						System.out.print(descriptor.get($argtype.text));
					}
	// -----------------------------------------------------------------------------------------------------
				)*
			)? 
	// -----------------------------------------------------------------------------------------------------
	// )
	//
		')' 
		{
			// Todo construtor retorna vazio
			sig_construct+= ")V"; 
			System.out.print(")V\n");
			gb_construtores.put($clname.text,sig_construct);
		}
	// -----------------------------------------------------------------------------------------------------
	// { super(f'); 
	//
	'{' 
		'super' '(' (ID {numsuperargs++;} (',' ID {numsuperargs++;} )*)? ')' ';' 
		{
			String supclass;
			if ((supclass=ct.get($clname.text)) != null) 
			{
				if (supclass.equals("Object")) 
				{
					int na = numargs+1;
					// TODO: Melhorar o calculo
					System.out.print("\t.limit stack "+ (na+5) +"\n");
					System.out.print("\t.limit locals "+ na +"\n");
					System.out.print("\taload_0\n");
					for (int a = 1; a<=numsuperargs; a++)
					{
						System.out.print("\taload " + a + "\n");
					}
					System.out.print("\tinvokenonvirtual java/lang/Object/<init>()V\n");
				}
				else 
				{
					int na = numargs+1;
					System.out.print("\t.limit stack "+ na +"\n"); // TODO: Melhorar o calculo
					System.out.print("\t.limit locals "+ na +"\n");
					System.out.print("\taload_0\n");
					for (int a = 1; a<=numsuperargs; a++)
					{
						System.out.print("\taload " + a + "\n");
					}
					System.out.print("\tinvokenonvirtual " + gb_construtores.get(supclass) +"\n");
				}
			}
		;}
	// -----------------------------------------------------------------------------------------------------
	// this.f' = f';
	//
		( // para cada atributo da classe :
			// O construtor inicializa os atributos da classe.
			'this' '.' attrib=ID '=' attvalue=ID ';'
			{
				int indexarg;
				
				System.out.print("\taload_0\n"); // this
				indexarg = k_args.indexOf($attvalue.text); 
				System.out.print("\taload " + indexarg + "\n");
				System.out.print("\tputfield " + signature.get($attrib.text) + " " + descriptor.get($attrib.text) + "\n"); 
			}
		)*
	// -----------------------------------------------------------------------------------------------------
	// }
	'}'
	{
		// Final do metodo.
		System.out.print("\treturn\n");
		System.out.print(".end method\n\n");
	}	
	// -----------------------------------------------------------------------------------------------------
;
// -----------------------------------------------------------------------------------------------------
// ----------------------------------- regra metodo(m) -------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// O metodo deve receber a classe a qual pertence para conseguir acessar os atributos
m[String supclass]
@init {
	int numargs = 0;
	m_args.clear(); 
	m_args.add("this");
	String sig_method = "";
}
: 
	// -----------------------------------------------------------------------------------------------------
	// C m
	//
	method_type=C method_name=ID 
	{	
		System.out.print(".method public " + $method_name.text);
		
		sig_method = "";
		sig_method += signature.get(supclass) + "/" + $method_name.text;
	}
	// -----------------------------------------------------------------------------------------------------
	// (
	//
	'(' 
	{
		sig_method += "(";	
		System.out.print("(");
	}
	// -----------------------------------------------------------------------------------------------------
	(
	// -----------------------------------------------------------------------------------------------------
	// D' x'
	//
	arg_type=C arg=ID 
	{
		m_args.add($arg.text); 
		numargs++;
		sig_method += descriptor.get($arg_type.text);
		System.out.print(descriptor.get($arg_type.text));
	}
	(
	// -----------------------------------------------------------------------------------------------------
	// , D' x'
	//
	',' arg_type=C arg=ID 
	{
		m_args.add($arg.text); 
		numargs++;
		sig_method += descriptor.get($arg_type.text);
		System.out.print(descriptor.get($arg_type.text));
	}
	)*
	)? 
	// -----------------------------------------------------------------------------------------------------
	// )
	//
	')'
	{
		sig_method += ")";
		System.out.print(")");
		sig_method += descriptor.get($method_type.text);
		System.out.print(descriptor.get($method_type.text));
		System.out.print("\n");
		gb_metodos.put($method_name.text,sig_method);
		
		// Adicionado em 2018 ***
		int na = numargs+1;
		System.out.print("\t.limit stack "+ (na+5) +"\n"); // TODO: Melhorar o calculo
		System.out.print("\t.limit locals "+ na +"\n");

	}
	// -----------------------------------------------------------------------------------------------------
	// { return e;}
	'{'
		'return' t ';'
		// Final do metodo. 
		{
			// Todo metodo em FW java, que não é o construtor, retorna um objeto.
			System.out.print("\tareturn\n");
			System.out.print(".end method\n\n");
		}
	'}'
	// -----------------------------------------------------------------------------------------------------

;
// -----------------------------------------------------------------------------------------------------
// ----------------------------------- regra assinatura do metodo(ms) ----------------------------------
// -----------------------------------------------------------------------------------------------------
// O metodo deve receber a classe a qual pertence para conseguir acessar os atributos
ms
@init {
	String sig_method = "";
}
//
// C m (C' x');
//
// method_type=C method_name=ID '(' (arg_type=C arg=ID (',' arg_type=C arg=ID)*)? ')' ';'
//
// Example: void foo();
// Saida: 	.method abstract foo()V
//			.end method
//
: 
	// ----------------------------------------------------------------------------------------
	// C m 
	method_type=C method_name=ID 
	{	
		System.out.print(".method abstract " + $method_name.text);
		
		sig_method = "";
		sig_method += $method_name.text;
	}
	// ----------------------------------------------------------------------------------------
	// ( 
	'(' 
	{
		sig_method += "(";	
		System.out.print("(");
	}
	// ----------------------------------------------------------------------------------------
	// C x
	(
	arg_type=C arg=ID 
	{
		// m_args.add($arg.text); 
		// numargs++;
		if (descriptor.get($arg_type.text) != null)
		{
			sig_method += descriptor.get($arg_type.text);
			System.out.print(descriptor.get($arg_type.text));	
		}
		else
		{
			sig_method += "L" + $arg_type.text + ";";
			System.out.print("L" + $arg_type.text + ";");
		}


	}
	// ----------------------------------------------------------------------------------------
	// C' x'
	(
	',' arg_type=C arg=ID 
	{
		// m_args.add($arg.text); 
		// numargs++;
		if (descriptor.get($arg_type.text) != null)
		{
			sig_method += descriptor.get($arg_type.text);
			System.out.print(descriptor.get($arg_type.text));	
		}
		else
		{
			sig_method += "L" + $arg_type.text + ";";
			System.out.print("L" + $arg_type.text + ";");
		}
	}
	)*
	)? 
	// ----------------------------------------------------------------------------------------
	// )
	')'
	{
		sig_method += ")";
		System.out.print(")");


		if (descriptor.get($method_type.text) != null)
		{
			sig_method += descriptor.get($method_type.text);
			System.out.print(descriptor.get($method_type.text));
		}
		else
		{
			sig_method += "L" + $method_type.text + ";";
			System.out.print("L" + $method_type.text + ";");
		}

		System.out.print("\n");
	}
	// ----------------------------------------------------------------------------------------
	// ;
	';'
	{
		System.out.print(".end method\n\n");
	}
	// ----------------------------------------------------------------------------------------
;
// -----------------------------------------------------------------------------------------------------
// ----------------------------------- regra termo(e) --------------------------------------------------
// -----------------------------------------------------------------------------------------------------
t
: 
	// -----------------------------------------------------------------------------------------------------
	// e ::= e.f
	//
	t '.' atributo=ID
	{
		// t -> objectref
		// getfield	sig(C)/ID desc(ID)
		// Considera que não existe atributos com mesmo nome em diferentes classes
		System.out.print("\tgetfield "+ signature.get($atributo.text) + " " + descriptor.get($atributo.text));
		System.out.print("\n");

	}
	// -----------------------------------------------------------------------------------------------------
|	
	// -----------------------------------------------------------------------------------------------------
	// e ::= e.m(e')
	//
	t '.' method_inv=ID '(' (t (',' t)*)? ')'
	{
		// invokestatic pois em FWJava a chamada de método é de uma classe
		// Alterado para invokevirtual pois é a chamada de um metodo de um objeto e nao de uma classe
		System.out.print("\tinvokevirtual " + gb_metodos.get($method_inv.text) + "\n");
	}
	// -----------------------------------------------------------------------------------------------------
|	
	// -----------------------------------------------------------------------------------------------------
	// invoke
	// e ::= new C(e')
	//
	'new' new_class=C  
	{
		System.out.print("\tnew "+ $new_class.text + "\n"); // cria referencia/objeto
		System.out.print("\tdup\n"); // duplica a referencia para executar o construtor
	}
	'(' (t (',' t)*)? ')'
	{
		// resolve t recursivamente
		System.out.print("\tinvokenonvirtual " + gb_construtores.get($new_class.text) + "\n");
	}
	// -----------------------------------------------------------------------------------------------------
|	
	// -----------------------------------------------------------------------------------------------------
	// e ::= (C) e
	//
	'(' cast=C ')' t
	{
		System.out.print("\tcheckcast " + signature.get($cast.text) + "\n"); // sig(C)
	}
	// -----------------------------------------------------------------------------------------------------

|	
// -----------------------------------------------------------------------------------------------------
	// adicionado pois como o this foi declarado implicitamente na regra k o ID nao da pattern em this.
	'this'
	{
		System.out.print("\taload_0\n");
	}
	// -----------------------------------------------------------------------------------------------------
|	
	// -----------------------------------------------------------------------------------------------------
	// e ::= x (Variavel local)
	//
	variavel=ID 
	{
		System.out.print("\taload " + m_args.indexOf($variavel.text) + "\n");
	}
	// -----------------------------------------------------------------------------------------------------
;
// -----------------------------------------------------------------------------------------------------
// -------------------------------- grammar lexical ----------------------------------------------------
// -----------------------------------------------------------------------------------------------------

C 		: [A-Z][a-z]*			; // Class identifier
ID 		: [a-z]([A-Z]|[a-z])*	; // Arg identifier
WS 		: [ \t\r\n]+ -> skip 	; // skip spaces, tabs, newlines

